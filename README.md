# Broken template ID

This repository contains a simple solution for fixing the broken template IDs 
for catalog items inside the CloudForms/ManageIQ. This problem is produced by 
recreating one or more templates with using the same name as was before on the 
provider site. In this case your catalog items will be pointing to the old 
(deleted) templates and without this fix you have to manually repick your 
recreated templates for every broken catalog item. 

# How it works?

Our code validates all of your existing Catalog Items and their related templates.
If there is an item pointing to the invalid template, it tries to find a new 
template with the same name and updates the ID. This search is limited by using 
the same provider. If there is no new template, you will be noted about the 
problematic item name.

## Steps:

1) Download the `rails_console/broken-template-id-fix.rb` file. 
2) Open your terminal and go into your CloudForms/ManageIQ folder.
3) Start the rails console and load(run) our file by using the commands bellow:

```
bundle exec rails c
load 'path/to/the/broken-template-id-fix.rb'

```
4) You will see the output, which contains summary of the modified methods

# future goals

We want to transform this ruby file into the custom button on the provider
page and be able to run it directly from the UI.