# validate the name and state of the given template
def template_valid?(template, template_name)
  return template.name == template_name && !template.archived? && !template.orphaned?
end

# find template ID on specific provider by name
def find_template_id(template_name, ems_id)
  ExtManagementSystem.find_by(:id => ems_id)&.miq_templates&.select { |t| template_valid?(t, template_name) }&.first&.id
end

# update config_info for the given template
def update_service_resources(config_info, service_template)
  service_template.send(:update_service_resources, config_info, "Admin")
end

sum = ""

ServiceTemplate.all.each do |st|
  # skip non-atomic services(bundles)
  next if st.service_type != 'atomic'

  ci = st.config_info
  template_id, template_name = ci[:src_vm_id]
  ems_id = ci[:src_ems_id][0]

  # skip if template is valid
  next if template_valid?(MiqTemplate.find_by(:id => template_id), template_name)

  # find a new template ID
  if new_id = find_template_id(template_name, ems_id)
    ci[:src_vm_id][0] = new_id
    sum += " * ServiceTemplate '#{st.name}' => Template '#{template_name}': ID='#{template_id}' was changed to ID='#{new_id}'\n"
    update_service_resources(ci, st)
  else
    sum += " * ServiceTemplate '#{st.name}' => Template '#{template_name}': There is no template with this name!'\n"
  end
end

puts "Summary:"
puts " * No template with wrong ID was found * " if sum == ""
puts sum